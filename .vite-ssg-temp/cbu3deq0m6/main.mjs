import { ViteSSG } from "vite-ssg/single-page";
import { useSSRContext, mergeProps, withCtx, createTextVNode, renderSlot, defineComponent, toDisplayString, unref, createVNode } from "vue";
import { ssrRenderAttrs, ssrRenderSlot, ssrRenderComponent, ssrInterpolate, ssrRenderList, ssrRenderStyle } from "vue/server-renderer";
const _export_sfc = (sfc, props) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props) {
    target[key] = val;
  }
  return target;
};
const _sfc_main$d = {};
function _sfc_ssrRender$3(_ctx, _push, _parent, _attrs) {
  _push(`<h1${ssrRenderAttrs(mergeProps({ class: "cv-h1" }, _attrs))} data-v-294f5d62>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</h1>`);
}
const _sfc_setup$d = _sfc_main$d.setup;
_sfc_main$d.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/atom/CvHeader1.vue");
  return _sfc_setup$d ? _sfc_setup$d(props, ctx) : void 0;
};
const CvHeader1 = /* @__PURE__ */ _export_sfc(_sfc_main$d, [["ssrRender", _sfc_ssrRender$3], ["__scopeId", "data-v-294f5d62"]]);
const _sfc_main$c = {
  __name: "CvTitle",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "title-wrapper" }, _attrs))} data-v-7a5ab898><div class="title-infos-wrapper" data-v-7a5ab898>`);
      _push(ssrRenderComponent(CvHeader1, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`FABIEN`);
          } else {
            return [
              createTextVNode("FABIEN")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(CvHeader1, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`CONTOLI`);
          } else {
            return [
              createTextVNode("CONTOLI")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`<div class="title-infos" data-v-7a5ab898><a class="info-link" href="https://github.com/FabienCont" data-v-7a5ab898>github: FabienCont</a><p data-v-7a5ab898>contoli.fabien@gmail.com</p><p data-v-7a5ab898>80 boulevard valbenoite, 42100 Saint Etienne</p><p data-v-7a5ab898>30 ans Permis B</p><p data-v-7a5ab898>Anglais professionnel</p></div></div><div class="title-infos-wrapper title-infos-wrapper-end" data-v-7a5ab898>`);
      _push(ssrRenderComponent(CvHeader1, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`DEVELOPPEUR`);
          } else {
            return [
              createTextVNode("DEVELOPPEUR")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(CvHeader1, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`FULLSTACK`);
          } else {
            return [
              createTextVNode("FULLSTACK")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`<div class="title-infos" data-v-7a5ab898><p class="info-bold" data-v-7a5ab898>2014 - 2017 Diplôme d&#39;ingénieur</p><p data-v-7a5ab898>EFREI - ASYRIA, Villejuif</p><p class="info-bold" data-v-7a5ab898>2012-1014 DUT GEII</p><p data-v-7a5ab898>Université Paris XIII, Villetaneuse</p></div></div></div>`);
    };
  }
};
const _sfc_setup$c = _sfc_main$c.setup;
_sfc_main$c.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvTitle.vue");
  return _sfc_setup$c ? _sfc_setup$c(props, ctx) : void 0;
};
const CvTitle = /* @__PURE__ */ _export_sfc(_sfc_main$c, [["__scopeId", "data-v-7a5ab898"]]);
const _sfc_main$b = {};
function _sfc_ssrRender$2(_ctx, _push, _parent, _attrs) {
  _push(`<h2${ssrRenderAttrs(mergeProps({ class: "cv-h2" }, _attrs))} data-v-5e139ef9>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</h2>`);
}
const _sfc_setup$b = _sfc_main$b.setup;
_sfc_main$b.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/atom/CvHeader2.vue");
  return _sfc_setup$b ? _sfc_setup$b(props, ctx) : void 0;
};
const CvHeader2 = /* @__PURE__ */ _export_sfc(_sfc_main$b, [["ssrRender", _sfc_ssrRender$2], ["__scopeId", "data-v-5e139ef9"]]);
const _sfc_main$a = {
  __name: "CvExperienceHeader",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "cv-experiences-header" }, _attrs))} data-v-e0017762>`);
      _push(ssrRenderComponent(CvHeader2, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            ssrRenderSlot(_ctx.$slots, "default", {}, null, _push2, _parent2, _scopeId);
          } else {
            return [
              renderSlot(_ctx.$slots, "default", {}, void 0, true)
            ];
          }
        }),
        _: 3
      }, _parent));
      _push(`</div>`);
    };
  }
};
const _sfc_setup$a = _sfc_main$a.setup;
_sfc_main$a.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvExperienceHeader.vue");
  return _sfc_setup$a ? _sfc_setup$a(props, ctx) : void 0;
};
const CvExperienceHeader = /* @__PURE__ */ _export_sfc(_sfc_main$a, [["__scopeId", "data-v-e0017762"]]);
const _sfc_main$9 = /* @__PURE__ */ defineComponent({
  __name: "CvExperienceSection",
  __ssrInlineRender: true,
  props: {
    experience: {
      type: Object,
      required: true
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "experience-section" }, _attrs))} data-v-0046633d><div class="experience-header" data-v-0046633d><div data-v-0046633d><p class="bold" data-v-0046633d>${ssrInterpolate(__props.experience.years)}</p><p class="bold" data-v-0046633d>${ssrInterpolate(__props.experience.duration)}</p></div><div data-v-0046633d><p class="bold" data-v-0046633d>${ssrInterpolate(__props.experience.job)}</p><p data-v-0046633d><span class="bold" data-v-0046633d>${ssrInterpolate(`${__props.experience.company}, ${__props.experience.city} (${__props.experience.country})`)}</span></p></div></div><div data-v-0046633d></div><div class="experience-stack" data-v-0046633d> Stack : <span data-v-0046633d>${ssrInterpolate(__props.experience.stacks.join(", "))}</span></div><!--[-->`);
      ssrRenderList(__props.experience.infos, (experienceInfo, i) => {
        _push(`<div class="experience-description" data-v-0046633d> - ${ssrInterpolate(experienceInfo.description)} `);
        if (experienceInfo.details && experienceInfo.details.length > 0) {
          _push(`<!--[-->`);
          ssrRenderList(experienceInfo.details, (detail, index) => {
            _push(`<p class="experience-detail" data-v-0046633d>${ssrInterpolate(detail)}</p>`);
          });
          _push(`<!--]-->`);
        } else {
          _push(`<!---->`);
        }
        _push(`</div>`);
      });
      _push(`<!--]--></div>`);
    };
  }
});
const _sfc_setup$9 = _sfc_main$9.setup;
_sfc_main$9.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvExperienceSection.vue");
  return _sfc_setup$9 ? _sfc_setup$9(props, ctx) : void 0;
};
const CvExperienceSection = /* @__PURE__ */ _export_sfc(_sfc_main$9, [["__scopeId", "data-v-0046633d"]]);
const skills = [
  {
    "category": "Front-End",
    "description": "HTML5, CSS3, Vue.js, karma, marionetteJs, BackboneJS, React, Jest"
  },
  {
    "category": "Outils développement",
    "description": "Atom, Intellij, webpack, yeo-man, Git, Jenkins, A4C"
  },
  {
    "category": "Gestion projets",
    "description": "Méthode agile, JIRA, Jive, Splunk, Kibana"
  }
];
const _sfc_main$8 = {};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs) {
  _push(`<h3${ssrRenderAttrs(mergeProps({ class: "cv-h3" }, _attrs))} data-v-d3a551dc>`);
  ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
  _push(`</h3>`);
}
const _sfc_setup$8 = _sfc_main$8.setup;
_sfc_main$8.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/atom/CvHeader3.vue");
  return _sfc_setup$8 ? _sfc_setup$8(props, ctx) : void 0;
};
const CvHeader3 = /* @__PURE__ */ _export_sfc(_sfc_main$8, [["ssrRender", _sfc_ssrRender$1], ["__scopeId", "data-v-d3a551dc"]]);
const _sfc_main$7 = {
  __name: "CvHorizontalSeparator",
  __ssrInlineRender: true,
  props: {
    absolute: {
      type: Boolean,
      default: false
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({
        class: [{ "cv-absolute-separator": __props.absolute }, "cv-horizontal-separator"]
      }, _attrs))} data-v-a5b27583></div>`);
    };
  }
};
const _sfc_setup$7 = _sfc_main$7.setup;
_sfc_main$7.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/atom/CvHorizontalSeparator.vue");
  return _sfc_setup$7 ? _sfc_setup$7(props, ctx) : void 0;
};
const CvHorizontalSeparator = /* @__PURE__ */ _export_sfc(_sfc_main$7, [["__scopeId", "data-v-a5b27583"]]);
const _sfc_main$6 = /* @__PURE__ */ defineComponent({
  __name: "CvSkillInfo",
  __ssrInlineRender: true,
  props: {
    skill: Object,
    separator: Boolean
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "cv-skill-wrapper" }, _attrs))} data-v-94eeb269>`);
      if (__props.separator == true) {
        _push(ssrRenderComponent(CvHorizontalSeparator, null, null, _parent));
      } else {
        _push(`<!---->`);
      }
      _push(ssrRenderComponent(CvHeader3, { class: "cv-skill-header" }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`${ssrInterpolate(__props.skill.category)}`);
          } else {
            return [
              createTextVNode(toDisplayString(__props.skill.category), 1)
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`<p class="cv-skill-description" data-v-94eeb269>${ssrInterpolate(__props.skill.description)}</p></div>`);
    };
  }
});
const _sfc_setup$6 = _sfc_main$6.setup;
_sfc_main$6.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvSkillInfo.vue");
  return _sfc_setup$6 ? _sfc_setup$6(props, ctx) : void 0;
};
const CvSkillInfo = /* @__PURE__ */ _export_sfc(_sfc_main$6, [["__scopeId", "data-v-94eeb269"]]);
const _sfc_main$5 = /* @__PURE__ */ defineComponent({
  __name: "CvSkillsSection",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "cv-skills-section" }, _attrs))} data-v-929032b1>`);
      _push(ssrRenderComponent(CvSkillInfo, {
        class: "cv-skills-info",
        skill: unref(skills)[0]
      }, null, _parent));
      _push(`<div class="cv-skills-separator" data-v-929032b1>`);
      _push(ssrRenderComponent(CvHorizontalSeparator, null, null, _parent));
      _push(`</div>`);
      _push(ssrRenderComponent(CvSkillInfo, {
        class: "cv-skills-info",
        style: { "position": "relative" },
        skill: unref(skills)[1]
      }, null, _parent));
      _push(`<div class="cv-skills-separator" data-v-929032b1>`);
      _push(ssrRenderComponent(CvHorizontalSeparator, null, null, _parent));
      _push(`</div>`);
      _push(ssrRenderComponent(CvSkillInfo, {
        class: "cv-skills-info",
        style: { "position": "relative" },
        skill: unref(skills)[2]
      }, null, _parent));
      _push(`</div>`);
    };
  }
});
const _sfc_setup$5 = _sfc_main$5.setup;
_sfc_main$5.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvSkillsSection.vue");
  return _sfc_setup$5 ? _sfc_setup$5(props, ctx) : void 0;
};
const CvSkillsSection = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["__scopeId", "data-v-929032b1"]]);
const projects = [
  {
    "description": "Participation à deux concours de développement de jeux vidéo en 48h sur « ldjam.com »."
  },
  {
    "description": "Réalisation de deux micros-jeux web en javascript."
  },
  {
    "description": "Réalisation de plusieurs pages web ( synchroniseur de sous titres, sudoku solveur, rubiks cube...)."
  }
];
const _sfc_main$4 = {
  __name: "CvProjectsSection",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "cv-project-section" }, _attrs))} data-v-3275ff9f><!--[-->`);
      ssrRenderList(unref(projects), (project, index) => {
        _push(`<p data-v-3275ff9f>${ssrInterpolate(project.description)}</p>`);
      });
      _push(`<!--]--></div>`);
    };
  }
};
const _sfc_setup$4 = _sfc_main$4.setup;
_sfc_main$4.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvProjectsSection.vue");
  return _sfc_setup$4 ? _sfc_setup$4(props, ctx) : void 0;
};
const CvProjectsSection = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["__scopeId", "data-v-3275ff9f"]]);
const _sfc_main$3 = /* @__PURE__ */ defineComponent({
  __name: "CvSectionWrapper",
  __ssrInlineRender: true,
  props: {
    separator: {
      type: Boolean,
      default: false
    }
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "cv-section-wrapper" }, _attrs))} data-v-bd594cbe>`);
      if (__props.separator == true) {
        _push(ssrRenderComponent(CvHorizontalSeparator, { absolute: true }, null, _parent));
      } else {
        _push(`<!---->`);
      }
      _push(`<div class="cv-section-content" data-v-bd594cbe>`);
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(`</div></div>`);
    };
  }
});
const _sfc_setup$3 = _sfc_main$3.setup;
_sfc_main$3.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvSectionWrapper.vue");
  return _sfc_setup$3 ? _sfc_setup$3(props, ctx) : void 0;
};
const CvSectionWrapper = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["__scopeId", "data-v-bd594cbe"]]);
const _sfc_main$2 = {};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs) {
  _push(`<!--[--><div class="print break-page" data-v-622f1e79></div><div class="cv-page-separator web" data-v-622f1e79></div><div style="${ssrRenderStyle({ "margin-top": "2rem" })}" data-v-622f1e79></div><!--]-->`);
}
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/atom/CvPageSeparator.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const CvPageSeparator = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["ssrRender", _sfc_ssrRender], ["__scopeId", "data-v-622f1e79"]]);
const _sfc_main$1 = /* @__PURE__ */ defineComponent({
  __name: "CvPageWrapper",
  __ssrInlineRender: true,
  props: {
    break: Boolean
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "cv-page-wrapper" }, _attrs))} data-v-e95020d3>`);
      if (__props.break) {
        _push(ssrRenderComponent(CvPageSeparator, { style: { "padding-bottom": "2rem" } }, null, _parent));
      } else {
        _push(`<!---->`);
      }
      ssrRenderSlot(_ctx.$slots, "default", {}, null, _push, _parent);
      _push(`</div>`);
    };
  }
});
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/components/cv/CvPageWrapper.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const CvPageWrapper = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["__scopeId", "data-v-e95020d3"]]);
const experiences = [
  {
    years: "2022 - 2023",
    duration: "18 mois",
    job: "Développeur Front End",
    company: "Groupama",
    city: "Ecully",
    country: "France",
    stacks: ["VueJS", "Vite", "Jest", "Java", "Micronaut", "Reactor", "JUnit"],
    infos: [
      {
        description: "Développement de services backend avec le framework reactor"
      },
      {
        description: "Mise en place de plusieurs microfronts via le framework single-spa"
      },
      {
        description: "Communication entre les microfronts grâce à l'observer pattern"
      },
      {
        description: "Développement d'une librairie partagée de composant réutilisable entre les microfronts"
      },
      {
        description: "Couverture de test unitaire sur tout le projet à 80% avec relécture par des pairs à chaque pull request"
      },
      {
        description: "Participation à des KATA, développement en peer programming"
      },
      {
        description: "partage/validation de la rédaction de la conception avant le développement"
      }
    ]
  },
  {
    years: "2021",
    duration: "4 mois",
    job: "Développeur Frontend",
    company: "Doing",
    city: "Saint Etienne",
    country: "France",
    stacks: ["VueJS", "Webpack", "Cordova", "Storybook"],
    infos: [
      {
        description: "Développement d’un site pour des salles multisports connéctés",
        details: [
          "Planning de réservation",
          "Enregistrement de carte de paiement via l’api payzen"
        ]
      },
      {
        description: "Création d’un container Cordova pour embarqué le site dans une application mobile"
      },
      {
        description: "Création d’une librairie de composant atomic"
      },
      {
        description: "Formation Flutter pour l’appli mobile"
      }
    ]
  },
  {
    years: "2019 - 2020",
    duration: "7 mois",
    job: "Développeur Frontend",
    company: "L'Oréal",
    city: "Clichy ",
    country: "France",
    stacks: ["VueJS", "Webpack", "graphQL", "C#", "ISS"],
    infos: [
      {
        description: "Création d’un site en interne pour gérer toutes les instances des sites vitrines de L’Oreal"
      },
      {
        description: "Création d’une librairie de composants atomiques Vuejs via Storybook"
      },
      {
        description: "Création de composants vanillaJs customisables pour les sites vitrines",
        details: [
          "Affichage des définitions des composants d’un produit via l’appel à un service de dictionnaire",
          "Affichage d’un composant HTML lors d’un stream Youtube de promotion"
        ]
      },
      {
        description: "Participation à un Hackathon avec l’Ecole 42"
      }
    ]
  },
  {
    years: "2016 - 2019",
    duration: "40 mois",
    job: "Développeur Frontend",
    company: "Société Générale",
    city: "Val-de-Fontenay",
    country: "France",
    stacks: ["ES6", "VueJS", "MarionetteJS", "karmaJS", "Webpack", "JaveEE", "Maven", "JUnit"],
    infos: [
      {
        description: "Création de la nouvelle page d’accueil du site Crédit du Nord (dashboard paramétrable par le client)",
        details: [
          "Création d’un template de widget via Yeoman"
        ]
      },
      {
        description: "Optimisation de la stack existante",
        details: [
          "RequireJs -> Webpack",
          "Weblogic -> JBoss",
          "POC/Etude Comparative React, VueJs, Angular au sein d’une feature team Lead Front",
          "Ajout de VueJs dans la Stack",
          "Analyse et optimisation du build via webpack"
        ]
      },
      {
        description: "Maintenance et suivi en production"
      },
      {
        description: "Mise en place d’une solution externe de FAQ"
      },
      {
        description: "Mise en place d’une solution d’Analytics maison"
      },
      {
        description: "Accompagnement projet pour d’autres équipes Crédit du Nord et Société Générale"
      }
    ]
  },
  {
    years: "2014 - 2016",
    duration: "20 mois",
    job: "Développeur Back-End",
    company: "Société Générale",
    city: "Val-de-Fontenay",
    country: "France",
    stacks: ["Java JEE", "JUnit", "aspectJ", "MAVEN", "Weblogic Server", "Weblogic Portal"],
    infos: [
      {
        description: "Maintenance et évolutions des applicatifs historiques du Site Crédit du Nord"
      },
      {
        description: "Conception et développement de nouveaux services"
      },
      {
        description: "Mise en place d’un environnement de test du site Crédit Du Nord"
      },
      {
        description: "Résolutions d’anomalies sur les applicatifs du site"
      },
      {
        description: "Optimisation de services existants"
      }
    ]
  },
  {
    years: "2014",
    duration: "3 mois",
    job: "Développeur d'application mobile",
    company: "ENSEA",
    city: "Cergy",
    country: "France",
    stacks: ["Java JEE", "JUnit", "aspectJ", "MAVEN", "Weblogic Server", "Weblogic Portal"],
    infos: [
      {
        description: "Création d’une application Android capable de contrôler un Roboquad"
      },
      {
        description: "Rétro-engineering d’un Robotquad initialement conçu pour être commandé via un contrôleur infrarouge"
      },
      {
        description: "Programmation du serveur sur une carte Arduino"
      },
      {
        description: "Connexion de la carte Arduino au Robot"
      }
    ]
  }
];
const _sfc_main = /* @__PURE__ */ defineComponent({
  __name: "App",
  __ssrInlineRender: true,
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<main${ssrRenderAttrs(_attrs)} data-v-4c8851cf>`);
      _push(ssrRenderComponent(CvPageWrapper, null, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(CvTitle, null, null, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvExperienceHeader, { style: { "margin-top": "1rem" } }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(`Competences`);
                } else {
                  return [
                    createTextVNode("Competences")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvSectionWrapper, null, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(ssrRenderComponent(CvSkillsSection, null, null, _parent3, _scopeId2));
                } else {
                  return [
                    createVNode(CvSkillsSection)
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvExperienceHeader, { class: "mb-2" }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(`Experiences 1 / 2`);
                } else {
                  return [
                    createTextVNode("Experiences 1 / 2")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvSectionWrapper, { separator: true }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(ssrRenderComponent(CvExperienceSection, {
                    experience: unref(experiences)[0]
                  }, null, _parent3, _scopeId2));
                  _push3(ssrRenderComponent(CvExperienceSection, {
                    experience: unref(experiences)[1]
                  }, null, _parent3, _scopeId2));
                  _push3(ssrRenderComponent(CvExperienceSection, {
                    experience: unref(experiences)[2]
                  }, null, _parent3, _scopeId2));
                } else {
                  return [
                    createVNode(CvExperienceSection, {
                      experience: unref(experiences)[0]
                    }, null, 8, ["experience"]),
                    createVNode(CvExperienceSection, {
                      experience: unref(experiences)[1]
                    }, null, 8, ["experience"]),
                    createVNode(CvExperienceSection, {
                      experience: unref(experiences)[2]
                    }, null, 8, ["experience"])
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
          } else {
            return [
              createVNode(CvTitle),
              createVNode(CvExperienceHeader, { style: { "margin-top": "1rem" } }, {
                default: withCtx(() => [
                  createTextVNode("Competences")
                ]),
                _: 1
              }),
              createVNode(CvSectionWrapper, null, {
                default: withCtx(() => [
                  createVNode(CvSkillsSection)
                ]),
                _: 1
              }),
              createVNode(CvExperienceHeader, { class: "mb-2" }, {
                default: withCtx(() => [
                  createTextVNode("Experiences 1 / 2")
                ]),
                _: 1
              }),
              createVNode(CvSectionWrapper, { separator: true }, {
                default: withCtx(() => [
                  createVNode(CvExperienceSection, {
                    experience: unref(experiences)[0]
                  }, null, 8, ["experience"]),
                  createVNode(CvExperienceSection, {
                    experience: unref(experiences)[1]
                  }, null, 8, ["experience"]),
                  createVNode(CvExperienceSection, {
                    experience: unref(experiences)[2]
                  }, null, 8, ["experience"])
                ]),
                _: 1
              })
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(ssrRenderComponent(CvPageWrapper, { break: true }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(CvExperienceHeader, { class: "mb-2" }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(`Experiences 2 / 2`);
                } else {
                  return [
                    createTextVNode("Experiences 2 / 2")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvSectionWrapper, { separator: true }, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(ssrRenderComponent(CvExperienceSection, {
                    experience: unref(experiences)[3]
                  }, null, _parent3, _scopeId2));
                  _push3(ssrRenderComponent(CvExperienceSection, {
                    experience: unref(experiences)[4]
                  }, null, _parent3, _scopeId2));
                  _push3(ssrRenderComponent(CvExperienceSection, {
                    experience: unref(experiences)[5]
                  }, null, _parent3, _scopeId2));
                } else {
                  return [
                    createVNode(CvExperienceSection, {
                      experience: unref(experiences)[3]
                    }, null, 8, ["experience"]),
                    createVNode(CvExperienceSection, {
                      experience: unref(experiences)[4]
                    }, null, 8, ["experience"]),
                    createVNode(CvExperienceSection, {
                      experience: unref(experiences)[5]
                    }, null, 8, ["experience"])
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvExperienceHeader, null, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(`Projets`);
                } else {
                  return [
                    createTextVNode("Projets")
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
            _push2(ssrRenderComponent(CvSectionWrapper, null, {
              default: withCtx((_2, _push3, _parent3, _scopeId2) => {
                if (_push3) {
                  _push3(ssrRenderComponent(CvProjectsSection, null, null, _parent3, _scopeId2));
                } else {
                  return [
                    createVNode(CvProjectsSection)
                  ];
                }
              }),
              _: 1
            }, _parent2, _scopeId));
          } else {
            return [
              createVNode(CvExperienceHeader, { class: "mb-2" }, {
                default: withCtx(() => [
                  createTextVNode("Experiences 2 / 2")
                ]),
                _: 1
              }),
              createVNode(CvSectionWrapper, { separator: true }, {
                default: withCtx(() => [
                  createVNode(CvExperienceSection, {
                    experience: unref(experiences)[3]
                  }, null, 8, ["experience"]),
                  createVNode(CvExperienceSection, {
                    experience: unref(experiences)[4]
                  }, null, 8, ["experience"]),
                  createVNode(CvExperienceSection, {
                    experience: unref(experiences)[5]
                  }, null, 8, ["experience"])
                ]),
                _: 1
              }),
              createVNode(CvExperienceHeader, null, {
                default: withCtx(() => [
                  createTextVNode("Projets")
                ]),
                _: 1
              }),
              createVNode(CvSectionWrapper, null, {
                default: withCtx(() => [
                  createVNode(CvProjectsSection)
                ]),
                _: 1
              })
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</main>`);
    };
  }
});
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("src/App.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const App = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-4c8851cf"]]);
const createApp = ViteSSG(App).mount("#app");
export {
  createApp
};
