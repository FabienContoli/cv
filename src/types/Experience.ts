export type ExperienceInfo = {
    description: String
    details?: Array<String>
}

export type Experience = {
    years:String,
    duration:String,
    job:String,
    company:String,
    city:String,
    country:String,
    stacks:Array<String>,
    infos:Array<ExperienceInfo>
}