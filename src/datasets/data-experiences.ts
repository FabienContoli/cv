import { Experience } from "@/types/Experience"

export default [
    {
        years:"2022 - 2023",
        duration:"18 mois",
        job:"Développeur Front End",
        company:"Groupama",
        city: "Ecully",
        country:"France",
        stacks:["VueJS", "Vite", "Jest", "Java", "Micronaut", "Reactor", "JUnit"],
        infos:[
            {
                description:"Développement de services backend avec le framework reactor",
            },
            {
                description:"Mise en place de plusieurs microfronts via le framework single-spa",
            },
            {
                description:"Communication entre les microfronts grâce à l'observer pattern",
            },
            {
                description:"Développement d'une librairie partagée de composant réutilisable entre les microfronts"
            },
            {
                description:"Couverture de test unitaire sur tout le projet à 80% avec relecture par des pairs à chaque pull request",
            },
            {
                description:"Participation à des KATA, développement en peer programming",
            },
            {
                description:"Partage puis validation de la rédaction de la conception avant le développement",
            },
        ]
    },
    {
        years:"2021",
        duration:"4 mois",
        job:"Développeur Frontend",
        company:"Doing",
        city: "Saint Etienne",
        country:"France",
        stacks:["VueJS", "Webpack", "Cordova", "Storybook"],
        infos:[
            {
                description:"Développement d’un site pour des salles multisports connéctées",
                details:[
                    "Planning de réservation",
                    "Enregistrement de carte de paiement via l’api payzen"
                ]
            },
            {
                description:"Création d’un conteneur Cordova pour embarquer le site dans une application mobile",
            },
            {
                description:"Création d’une librairie de composant atomic",
            },
            {
                description:"Formation Flutter pour l’appli mobile"
            }
        ]
    },
    {
        years:"2020 - 2021",
        duration:"16 mois",
        job:"Développeur Fullstack",
        company:"Projet Personnel",
        city: "Saint Etienne",
        country:"France",
        stacks:["Vanillajs","VueJS", "Webpack", "canvas API", "ECS","Docker"],
        infos:[
            {
                description:"Création d’un moteur de jeu 2d en vanilla js",
            },
            {
                description:"Création d’un éditeur de jeu par dessus le moteur en vuejs (frontend) et nodejs (backend)",
            },
            {
                description:"Participation à la Ldjam 48.",
                details:[ 
                    "Réalisation d’un jeu en 48h sous format d’une web app à l’aide de l’éditeur"
                ]
            },
            {
                description:"Création d’un loader et visualisateur de fichier gltf 3d vanillajs et via webgl"
            },
            {
                description:"Poc d’un editeur de niveau 3D avec des models 3D gltf importés"
            },
        ]
    },
    {
        years:"2019 - 2020",
        duration:"7 mois",
        job:"Développeur Frontend",
        company:"L'Oréal",
        city: "Clichy ",
        country:"France",
        stacks:["VueJS", "Webpack", "graphQL", "C#","ISS"],
        infos:[
            {
                description:"Création d’un site en interne pour gérer toutes les instances des sites vitrines de L’Oreal",
            },
            {
                description:"Création d’une librairie de composants atomiques Vuejs via Storybook",
            },
            {
                description:"Création de composants vanillaJs customisables pour les sites vitrines",
                details:[ 
                    "Affichage des définitions des ingrédients d’un produit via l’appel à un service de dictionnaire",
                    "Affichage d’un composant HTML lors d’un stream Youtube de promotion"
                ]
            },
            {
                description:"Participation à un Hackathon avec l’Ecole 42"
            }
        ]
    },
    {
        years:"2016 - 2019",
        duration:"40 mois",
        job:"Développeur Frontend",
        company:"Société Générale",
        city: "Val-de-Fontenay",
        country:"France",
        stacks:["ES6", "VueJS", "MarionetteJS", "karmaJS", "Webpack", "JaveEE", "Maven","JUnit"],
        infos:[
            {
                description:"Création de la nouvelle page d’accueil du site Crédit du Nord (dashboard paramétrable par le client)",
                details:[
                    "Création d’un template de widget via Yeoman"
                ]
            },
            {
                description:"Optimisation de la stack existante",
                details:[
                    "RequireJs -> Webpack",
                    "Weblogic -> JBoss",
                    "POC/Etude Comparative React, VueJs, Angular au sein d’une feature team Lead Front",
                    "Ajout de VueJs dans la Stack",
                    "Analyse et optimisation du build via webpack"
                ]
            },
            {
                description:"Maintenance et suivi en production",
            },
            {
                description:"Mise en place d’une solution externe de FAQ"
            },
            {
                description:"Mise en place d’une solution d’Analytics maison"
            },
            {
                description:"Accompagnement projet pour d’autres équipes Crédit du Nord et Société Générale"
            }
        ]
    },
    {
        years:"2014 - 2016",
        duration:"20 mois",
        job:"Développeur Back-End",
        company:"Société Générale",
        city: "Val-de-Fontenay",
        country:"France",
        stacks:["Java JEE", "JUnit", "aspectJ", "MAVEN", "Weblogic Server", "Weblogic Portal"],
        infos:[
            {
                description:"Maintenance et évolutions des applicatifs historiques du Site Crédit du Nord",
            },
            {
                description:"Conception et développement de nouveaux services",
            },
            {
                description:"Mise en place d’un environnement de test du site Crédit Du Nord",
            },
            {
                description:"Résolutions d’anomalies sur les applicatifs du site"
            },
            {
                description:"Optimisation de services existants"
            }
        ]
    }, 
    {
        years:"2014",
        duration:"3 mois",
        job:"Développeur d'application mobile",
        company:"ENSEA",
        city: "Cergy",
        country:"France",
        stacks:["Java JEE", "JUnit", "aspectJ", "MAVEN", "Weblogic Server", "Weblogic Portal"],
        infos:[
            {
                description:"Création d’une application Android capable de contrôler un Roboquad",
            },
            {
                description:"Rétro-engineering d’un Robotquad initialement conçu pour être commandé via un contrôleur infrarouge",
            },
            {
                description:"Programmation du serveur sur une carte Arduino",
            },
            {
                description:"Connexion de la carte Arduino au Robot"
            }
        ]
    }
] as Array<Experience>