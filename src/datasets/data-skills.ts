
import type { Skill } from "@/types/Skill"

export default [
        {
            "category": "Front-End",
            "description": "HTML5, CSS3, Vue.js, karma, marionetteJs, BackboneJS, React, Jest"
        },
        {
            "category": "Outils développement",
            "description": "Atom, Intellij, webpack, yeo-man, Git, Jenkins, A4C"
        },
        {
            "category": "Gestion projets",
            "description":"Méthode agile, JIRA, Jive, Splunk, Kibana"
        }
] as Array<Skill>