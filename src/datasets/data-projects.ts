import { Project } from "@/types/Project"

export default [
    {
        "description": "Participation à cinq concours de développement de jeux vidéo en 48h sur « ldjam.com »:",
        "link":"https://ldjam.com/users/fabonacci/games"
    },
    {
        "description": "2023: Bénévolat pour une association (Ressourcerie), réalisation de macros VBA leur permettant d'imprimer un catalogue"
    },
    {
        "description": "2022: Bénévolat pour l'association Dôme, Création d'un site web statique faisant la promotion du festival Echos"
    },
    {
        "description": "Réalisation de plusieurs pages web statiques ( synchroniseur de sous titres, sudoku solveur, rubiks cube...)."
    }
] as Array<Project>